import scrapy
import json
from project_scraper.items import InsolvencyDirectItem

class ZillowSpider(scrapy.Spider):
    name = "zillow"
    sale_url = 'https://www.zillow.com/search/GetSearchPageState.htm?searchQueryState={"pagination":{"currentPage":%(pg)s},"mapBounds":{"west":24.9493,"east":49.5904,"south":-125.0011,"north":-66.9326},"isMapVisible":false,"filterState":{"isForSaleByAgent":{"value":true},"isForSaleByOwner":{"value":true},"isNewConstruction":{"value":false},"isForSaleForeclosure":{"value":true},"isComingSoon":{"value":true},"isAuction":{"value":true},"isPreMarketForeclosure":{"value":false},"isPreMarketPreForeclosure":{"value":false},"isForRent":{"value":false},"isAllHomes":{"value":true}, "price":{"min":%(price_min)s,"max":%(price_max)s}},"isListVisible":true}&wants={"cat1":["listResults"]}&requestId=2'
    rent_url = 'https://www.zillow.com/search/GetSearchPageState.htm?searchQueryState={"pagination":' \
               '{"currentPage":%(pg)s},"mapBounds":{"west":24.9493,"east":49.5904,"south":-125.0011,"north":-66.9326},' \
               '"isMapVisible":false,"filterState":{"isForSaleByAgent":{"value":false},"isForSaleByOwner":' \
               '{"value":false},"isNewConstruction":{"value":false},"isForSaleForeclosure":{"value":false},' \
               '"isComingSoon":{"value":false},"isAuction":{"value":false},"isPreMarketForeclosure":{"value":false},' \
               '"isPreMarketPreForeclosure":{"value":false},"isForRent":{"value":true},"isAllHomes":{"value":true}, ' \
               '"monthlyPayment":{"min": 0,"max":100}},"isListVisible":true}&wants={"cat1":["listResults"]}&requestId=2'
    headers = {
        'authority': 'www.realtor.com',
        'pragma': 'no-cache',
        'cache-control': 'no-cache',
        'accept': 'application/json',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36',
        'content-type': 'application/json',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://www.realtor.com/realestateandhomes-detail/11738-Moorpark-St-Apt-D_Studio-City_CA_91604_M10906-39124?view=qv',
        'accept-language': 'el-GR,el;q=0.9,en;q=0.8,de;q=0.7,it;q=0.6',
    }

    rent_increasing_price = 100
    sale_increasing_price = 100
    end_price_sale = 300000000
    end_price_rent = 60000


    def start_requests(self):
        for i in range(0, self.end_price_sale, self.sale_increasing_price):
            yield scrapy.Request(self.sale_url % ({"pg": "1", "price_min": str(i),
                                                   "price_max": str(i+self.sale_increasing_price)}),
                                 headers=self.headers,
                                 meta={"pg": 1, "price_min": i, "price_max": i+self.sale_increasing_price},
                                 callback=self.parse)

        for i in range(0, self.end_price_rent, self.rent_increasing_price):
            yield scrapy.Request(self.sale_url % ({"pg": "1", "price_min": str(i),
                                                   "price_max": str(i+self.rent_increasing_price)}),
                                 headers=self.headers,
                                 meta={"pg": 1, "price_min": i, "price_max": i+self.rent_increasing_price},
                                 callback=self.parse)

    def parse(self, response):
        # print(response.body_as_unicode())
        data = json.loads(response.body_as_unicode())
        pg = response.meta["pg"]
        price_min = response.meta["price_min"]
        print_max = response.meta["price_max"]
        total_pages = data["cat1"]["searchList"]["totalPages"]
        for d in data["cat1"]["searchResults"]["listResults"]:
            itm = InsolvencyDirectItem()
            itm["object"] = d
            yield itm
        if pg < total_pages:
            yield scrapy.Request(self.sale_url % ({"pg": str(pg + 1), "price_min": price_min,
                                                   "price_max": print_max}),
                                 headers=self.headers,
                                 meta={"pg": pg + 1, "price_min": price_min, "price_max": print_max},
                                 callback=self.parse)
